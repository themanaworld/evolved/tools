#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
f=open("minimap-dyecmd.sh", "w");
CLIENT_DATA_ROOT="../../client-data"

map_names = sorted([os.path.splitext(p)[0] for p in os.listdir(os.path.join(CLIENT_DATA_ROOT, u'graphics', u'minimaps'))])

f.write('cd '+os.path.join(CLIENT_DATA_ROOT, u'graphics', u'minimaps')+'\n')
for c in map_names:
    if os.path.exists(os.path.join(CLIENT_DATA_ROOT, u'graphics', u'minimaps', c+u'.png')):
        i=c+u'.png'
        f.write("dyecmd %s tmp ; mv tmp %s\n" % (i, i))
        f.write("echo \"Converted "+i+" successfully.\"\n")

#f.write("git checkout -- minimap-dyecmd.sh")
f.close()
