#!/usr/bin/python2.7
import copy
TYPE_NUL=0
TYPE_WPN=1
TYPE_BOW=2
TYPE_SHD=3

class Item:
  def __init__(self, xid, typ=TYPE_NUL):
    self.id=xid
    self.lvl=0
    self.type=typ

def main(a, typ=TYPE_NUL):
  global swords, bows, shields, gid, rid, tip, ctx, mem
  gid="0"
  rid=0
  tip=TYPE_NUL
  ctx=Item(0, typ)
  for l in a:
    if "<item id=" in l:
        if ctx.id > 0:
            mem.append(copy.copy(ctx))

        gid=l.replace('\t', '').replace(' ','').replace('<itemid=', '').replace('"', '').replace("'", "")
        rid=0
        if "-" in gid:
            gid="0"
            continue
        try:
            rid=int(gid)
        except:
            print "[CRITICAL] Invalid item ID format: " + l
            exit(1)

        ctx=Item(rid, typ)

    if "\tlevel=" in l or " level=" in l:
        gid=l.replace('\t', '').replace(' ','').replace('level=', '').replace('"', '').replace("'", "")
        try:
            ctx.lvl=int(gid)
        except:
            print "[CRITICAL] Invalid item level format: " + l
            ctx.lvl=0

    if "\tattack-range=" in l or " attack-range=" in l:
        tip=l.replace('\t', '').replace(' ','').replace('attack-range=', '').replace('"', '').replace("'", "").replace(">", "")
        try:
            if int(tip) > 2:
                ctx.type=TYPE_BOW
            else:
                ctx.type=TYPE_WPN
        except:
            print "[CRITICAL] Invalid item range format: " + l
            ctx.type=TYPE_NUL

        ctx.lvl=0+rid
  return

swords=[]
bows=[]
shields=[]

mem=[]

f1=open("../../client-data/items/equip-1hand.xml", "r"); main(f1); f1.close()
f2=open("../../client-data/items/equip-2hand.xml", "r"); main(f2); f2.close()
f3=open("../../client-data/items/equip-shield.xml", "r"); main(f3, TYPE_SHD); f3.close()

mem=sorted(mem, key=lambda xcv: xcv.lvl, reverse=True)

for r in mem:
    if r.type == TYPE_SHD:
        shields.append(r.id)
    elif r.type == TYPE_WPN:
        swords.append(r.id)
    elif r.type == TYPE_BOW:
        bows.append(r.id)
    else:
        print("Wrong type for item %d" % r.id)

#shields=sorted(shields, reverse=True)
#bows=sorted(bows, reverse=True)
#swords=sorted(swords, reverse=True)

b=open("weapons.tmp", "w")

b.write('<?xml version="1.0" encoding="utf-8"?>\n\
<!-- Author: 4144, Jesusalva\n\
Copyright (C) 2015 Evol Online\n\
Copyright (C) 2019-2021 The Mana World\n -->\n\
\n\
<weapons>\n')

b.write('    <swords>\n')

for i in swords:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </swords>\n    <bows>\n')

for i in bows:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </bows>\n    <shields>\n')

for i in shields:
    b.write('        <item id="%d"/>\n' % i)

b.write('    </shields>\n</weapons>')

b.close()
